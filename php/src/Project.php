<?php

/**
 * Per test from Justin Etheridge:
 * 
Technical Exercise:

You have a set of projects, and you need to calculate a reimbursement amount for the set. Each project has a start date and an end date. The first day of a project and the last day of a project are always "travel" days. Days in the middle of a project are "full" days. There are also two types of cities a project can be in, high cost cities and low cost cities. 

A few rules:
First day and last day of a project, or sequence of projects, is a travel day.
Any day in the middle of a project, or sequence of projects, is considered a full day.
If there is a gap between projects, then the days on either side of that gap are travel days.
If two projects push up against each other, or overlap, then those days are full days as well.
Any given day is only ever counted once, even if two projects are on the same day.
A travel day is reimbursed at a rate of 45 dollars per day in a low cost city.
A travel day is reimbursed at a rate of 55 dollars per day in a high cost city.
A full day is reimbursed at a rate of 75 dollars per day in a low cost city.
A full day is reimbursed at a rate of 85 dollars per day in a high cost city.

 */
class Project {
	protected $startDate;
	function getStartDate() { return $this->startDate; }

	protected $endDate;
	function getEndDate() { return $this->endDate; }

	/**
	 * Determines cost of city for a project
	 * Currently only supports high or low
	 */
	protected $cityCost;

	/**
	 * Array of supported city costs, with rates for travel and full days. Adjust as needed
	 */
	protected $supportedCityCosts = [
						'low' => ['full'=>75, 'travel'=>45],
						'high' => ['full'=>85, 'travel'=>55],
				      	];
	function getFullRate() {
		return $this->supportedCityCosts[$this->cityCost]['full'];
	}
	function getTravelRate() {
		return $this->supportedCityCosts[$this->cityCost]['travel'];
	}

	function __construct($params) {
		// Start and End Dates
		if ($params['start_date']) {
			$this->startDate = new DateTime($params['start_date']);
		}	
		if ($params['end_date']) {
			$this->endDate = new DateTime($params['end_date']);
		}

		// But first! Check and see if end date is after or on start date, if not, fail!
		if ($this->startDate->diff($this->endDate)->format('%R%a') < 0) {
			throw new Exception('End date must be after start date');
		}

		if (!in_array($params['city_cost'],array_keys($this->supportedCityCosts))) {
			throw new Exception("Invalid city cost {$params['city_cost']}");
		}
		else {
			$this->cityCost = $params['city_cost'];
		}

	}

	/*
	 * List a project's dates and whether it's a full day or travel day
	 */
	function projectDates() {
		$dates = [];
		$period = new DatePeriod($this->startDate,new DateInterval('P1D'),new DateTime($this->endDate->format('c').' +1 day') );
		foreach ($period as $date) {
			$dates[] = $date;	
		}	
		return $dates;
	}
	
	static function calculateProjects(...$projects) {
		$dates = [];
		foreach ($projects as $project) {
			if ($project instanceof Project) {
				$projectDates = $project->projectDates();
				foreach ($projectDates as $date) {
					// If a beginning or end date (and not already set or adjacent to another project), or same date mark as travel;	
					if ( (!isset($dates[$date->format('Y-m-d')]) || !isset($dates[$date->format('Y-m-d').' -1 day'])) && ($project->getStartDate() == $date || $project->getEndDate() == $date) ) {
						$dates[$date->format('Y-m-d')] = 'travel';
					}
					// Otherwise, set as a full day
					else {
						$dates[$date->format('Y-m-d')] = 'full';
					}
				}
			}
		}

		// Get counts of full and travel days
		$days = array_count_values($dates);

		return [
			'full_rate' => $project->getFullRate() * $days['full'],
			'travel_rate' => $project->getTravelRate() * $days['travel'],
		];


	} 

}
